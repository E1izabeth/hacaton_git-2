from flask import render_template, request, send_from_directory
from app import app
from Arseniy import mytest
import copy
from consts import *

@app.route('/', methods=['GET', 'POST'])
def index():

	# tl = []
	# t = mytest.Tag()
	# for i in  range(10):
	# 	t.name = '#'+str(i)
	# 	t.avg_value = i
	# 	tl.append(copy.deepcopy(t))


	models_d_col = mytest.get_models()
	models_d = models_d_col.find()
	models = []
	for model_d in models_d:
		model = mytest.Model(model_dict=model_d)
		models.append(model)


	# a = mytest.Model()
	# a.name = "Name"
	# a.short_descr = "Koool"
	# a.list_tags = copy.deepcopy(tl)
	# a.likes_avg = 2
	# b = copy.deepcopy(a)
	# b.name = "Name2"
	# b.list_tags.pop()
	# b.list_tags.pop()
	# b.list_tags.pop()
	# c = copy.deepcopy(b)
	# c.name = "Name3"
	# b.list_tags.pop()
	# b.list_tags.pop()
	# b.list_tags.pop()
	# b.list_tags.pop()
	# b.list_tags.pop()
	# b.list_tags.pop()
	# models = [ a, b, c ] #TODO

	if request.method == 'GET':
		return render_template("index.html",
			title = 'Home',
			models = models)
	if request.method == 'POST':
		search = request.form['query']
		mtype = request.form['mtype']
		words = search[:search.find('#')].split() # TODO
		tags = search[search.find('#'):].split()

		end_models = []
		for x in models:
			if str(x.type) == str(mtype):
				end_models.append(x)

		relevant_sort(end_models, words, tags)
		return render_template("index.html",
			title = 'Home',
			search = search,
			models = end_models)

@app.route('/roadmap', methods=['GET', 'POST'])
def roadmap():
	model = mytest.Model()
	models = []
	if request.method == 'POST':
		id = request.form['button']
		if id == 'None':
			search = request.form['query']
			words = search[:search.find('#')].split() # TODO
			tags = search[search.find('#'):].split()
			models = get_relevant_models(tags)
		else:
			model = mytest.get_model_by_id(str(id))
	return render_template('roadmap.html',models = models, model = model, liststr = " ".join(x.name for x in model.list_tags))

def get_relevant_models(tags):
	models_d_col = mytest.get_models()
	models_d = models_d_col.find()
	models = []
	for model_d in models_d:
		model = mytest.Model(model_dict=model_d)
		if model.type == 1:
			models.append(model)

	end_models = []

	old_len = len(tags)
	relevant_sort(models, [' '], tags)
	end_models.append(models[0])
	for x in models[0].list_tags:
		if x in tags:
			tags.remove(x.name)
	while len(tags) != old_len:
		old_len = len(tags)
		relevant_sort(models, [' '], tags)
		end_models.append(models[0])
		for x in models[0].list_tags:
			if x in tags:
				tags.remove(x.name)

	return end_models


@app.route('/id/<id>')
def course(id):
	
	model = mytest.get_model_by_id(str(id))

	if model is not None:
		return render_template('course.html',
		id = id, model=model)

	return render_template('404.html')

@app.route('/res/<path:path>')
def send_file(path):
	return send_from_directory('res', path)


def relevant_sort(models, words, tags):
	models.sort(key=lambda x: relevant(x, words, tags), reverse=True)


def relevant(model, words, tags):
	relevant = K_LIKES*model.likes_avg
	for word in words:
		if model.long_descr.find(word) != -1:
			relevant += K_WORD
		if model.name.find(word) != -1:
			relevant += K_NAME
	if model.list_tags is not None:
		for tag in tags:
			for tagx in model.list_tags:
				if tag == tagx.name:
					relevant += K_TAG*tagx.avg_value
	return relevant
