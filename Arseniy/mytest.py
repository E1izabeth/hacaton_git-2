from bson import ObjectId
from pymongo import MongoClient


class Tag:
    def __init__(self, name='', avg_value=0.0, value_count=0, tag_dict=None):
        if tag_dict is None:
            self.name = name
            self.avg_value = avg_value
            self.value_count = value_count
        else:
            self.name = tag_dict['name']
            self.avg_value = tag_dict['avg_value']
            self.value_count = tag_dict['value_count']

    def to_dict(self):
        tag_dict = {'name': self.name, 'avg_value': self.avg_value, 'value_count': self.value_count}
        return tag_dict


class Model:
    def __init__(self, name='', short_descr='', long_descr='', model_dict=None):
        if model_dict is None:
            self.likes_count = 0
            self.likes_avg = 0.0
            self.list_tags = []
            self.short_descr = short_descr
            self.long_descr = long_descr
            self.diff_avg = 0.0
            self.diff_count = 0
            self.name = name
            self.relevant = 0
            self.id = -1
            self.type = 0
        else:
            self.name = model_dict['name']
            self.likes_count = int(model_dict['likes_count'])
            self.likes_avg = float(model_dict['likes_avg'])
            _list_tags = model_dict['list_tags']
            self.list_tags = []
            for tag in _list_tags:
                new_tag = Tag(tag_dict=tag)
                self.list_tags.append(new_tag)
            self.short_descr = model_dict['short_descr']
            self.long_descr = model_dict['long_descr']
            self.diff_avg = float(model_dict['diff_avg'])
            self.diff_count = int(model_dict['diff_count'])
            self.relevant = model_dict['relevant']
            self.id = model_dict['_id']
            self.type = int(model_dict['type'])

    def to_dict(self):
        model_dict = {'name': self.name, 'likes_count': self.likes_count, 'likes_avg': self.likes_avg,
                      'list_tags': self.list_tags, 'short_descr': self.short_descr, 'long_descr': self.long_descr,
                      'diff_avg': self.diff_avg, 'diff_count': self.diff_count,
                      'relevant': self.relevant, '_id': self.id, 'type': self.type}
        return model_dict


def get_models():
    client = MongoClient()
    db = client['SiteDB']
    models = db['Models']
    return models


def update_model(_id, diff_value, like_value):

    models = get_models()
    model_d = models.find({"_id": _id})
    model = Model(model_dict=model_d)
    if not((diff_value == 0) | (like_value == 0)):
        new_diff_value = model.diff_count * model.diff_avg + diff_value
        new_diff_count = model.diff_count + 1
        new_diff_avg = new_diff_value / new_diff_count
        new_likes_value = model.likes_count * model.likes_avg + like_value
        new_likes_count = model.likes_count + 1
        new_like_avg = new_likes_value / new_likes_count
    else:
        if diff_value == 0:
            new_diff_avg = model.diff_avg
            new_diff_count = model.diff_count
        if like_value == 0:
            new_like_avg = model.likes_avg
            new_likes_count = model.likes_count
    models.update_one(
        {"_id": _id},
        {"$set": {'diff_avg': new_diff_avg, 'diff_count': new_diff_count, 'like_avg': new_like_avg,
                  'likes_count': new_likes_count}}
    )


def set_new_model(model_dict=None, name=''):
    models = get_models()
    if model_dict is None:
        model_dict = {'name': name, 'likes_count': 1, 'likes_avg': 3,
                      'list_tags': [], 'short_descr': 'blabla', 'long_descr': 'ololo',
                      'diff_avg': 6, 'diff_count': 8, 'relevant': 0, 'type': 1}
    models.insert_one(model_dict)
    id = model_dict['_id']
    models.update_one(
        {"_id": id},
        {"$set": {"id": id}}
    )
    return id


def get_model_by_id(id):
    models = get_models()
    model_d = models.find_one({"_id": ObjectId(id)})
    # print(model_d['_id'])
    if model_d is None:
        print('No one model is gotten by this id')
        return None
    else:
        model = Model(model_dict=model_d)
        return model


def change_value(id, value_name, value):
    models_d = get_models()
    models_d.update_one(
        {"_id": ObjectId(id)},
        {"$set": {value_name: value}}
    )


def print_models():
    models = get_models()
    models_collection = models.find()
    for model_dict in models_collection:
        m = Model(model_dict=model_dict)
        print(m.name, m.short_descr, m.long_descr)


def update_list_tags(list_tags, new_tag_d):
    new_tag = Tag(tag_dict=new_tag_d)
    print(new_tag.name)
    is_matched = False
    for last_tag_d in list_tags:
        last_tag = Tag(tag_dict=last_tag_d)
        print(last_tag.name)
        if last_tag.name == new_tag.name:
            is_matched = True
            ind = list_tags.index(last_tag_d)
            last_tag = add_new_rate(last_tag, new_tag.avg_value)
            list_tags[ind] = last_tag.to_dict()
    if not is_matched:
        list_tags.append(new_tag)
    return list_tags


def add_new_rate(tag_d, value):
    tag = Tag(tag_dict=tag_d)
    new_value = tag.value_count * tag.avg_value + value
    tag.value_count += 1
    tag.avg_value = new_value / tag.value_count
    return tag


def print_tag(tag_d):
    tag = Tag(tag_dict=tag_d)
    print(tag.name, tag.avg_value, tag.value_count)


def main():
    models_d = get_models()
    models_d.drop()
    list_ids = []
    list_ids.append(set_new_model(name='C++ main developer'))
    list_ids.append(set_new_model(name='Python middle developer'))
    list_ids.append(set_new_model(name='Mathematics'))
    list_ids.append(set_new_model(name='Presentation'))
    list_ids.append(set_new_model(name='Data analyzing'))

    tag = Tag('#sql', 3, 1)
    tag2 = Tag('#threads', 2.2, 8)
    tag3 = Tag('#c++', 5.0, 2)
    tag4 = Tag('#python', 2.1, 3)
    tag5 = Tag('#low_level_programming', 4.3, 2)
    tag6 = Tag('#django', 4.7, 9)
    tag7 = Tag('#data_science', 5.0, 6)
    tag8 = Tag('#discrete_math', 3.6, 9)
    tag9 = Tag('#high_math', 5.0, 10)
    tag10 = Tag('#log', 4.1, 10)
    tag11 = Tag('#vba', 3.8, 10)
    tag12 = Tag('#wpf', 3.8, 10)
    tag13 = Tag('#win_forms', 3.8, 10)
    tag14 = Tag('#csharp', 3.8, 10)
    tag15 = Tag('#stl', 3.6, 10)
    tag16 = Tag('#include', 4, 10)


    lt1 = []
    lt1.append(tag.to_dict())
    lt1.append(tag2.to_dict())
    lt1.append(tag3.to_dict())
    lt1.append(tag5.to_dict())
    lt1.append(tag15.to_dict())
    lt1.append(tag16.to_dict())
    lt1.sort(key=lambda x: (x['avg_value']), reverse=True)


    lt2 = []
    lt2.append(tag2.to_dict())
    lt2.append(tag4.to_dict())
    lt2.append(tag6.to_dict())
    lt2.append(tag7.to_dict())
    lt2.sort(key=lambda x: (x['avg_value']), reverse=True)

    lt3 = []
    lt3.append(tag8.to_dict())
    lt3.append(tag9.to_dict())
    lt3.append(tag10.to_dict())
    lt3.sort(key=lambda x: (x['avg_value']), reverse=True)

    lt4 = []
    lt4.append((tag11.to_dict()))
    lt4.append((tag12.to_dict()))
    lt4.append((tag13.to_dict()))
    lt4.append((tag14.to_dict()))
    lt4.sort(key=lambda x: (x['avg_value']), reverse=True)

    lt5 = []
    lt5.append(tag.to_dict())
    lt5.append(tag2.to_dict())
    lt5.append(tag3.to_dict())
    lt5.append(tag4.to_dict())
    lt5.append(tag5.to_dict())
    lt5.append(tag7.to_dict())
    lt5.append(tag8.to_dict())
    lt5.append(tag9.to_dict())
    lt5.append(tag15.to_dict())
    lt5.sort(key=lambda x: (x['avg_value']), reverse=True)

    change_value(list_ids[0], 'list_tags', lt1)
    change_value(list_ids[1], 'list_tags', lt2)
    change_value(list_ids[2], 'list_tags', lt3)
    change_value(list_ids[3], 'list_tags', lt4)
    change_value(list_ids[4], 'list_tags', lt5)
    change_value(list_ids[0], 'type', 1)
    change_value(list_ids[1], 'type', 1)
    change_value(list_ids[2], 'type', 1)
    change_value(list_ids[3], 'type', 1)
    change_value(list_ids[4], 'type', 0)


main()
