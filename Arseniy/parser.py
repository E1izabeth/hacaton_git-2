import requests
from html.parser import HTMLParser
import re


class MyHTMLParser(HTMLParser):
    t = False

    def error(self, message):
        pass

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            if attrs[0][1].find("https://habrahabr.ru/search/?q=%5B") != -1:
                self.t = True

    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        if self.t:
            self.t = False
            if data != '' and data[0] != '' and data[0] != ' ':
                '''f = open('filter.txt', 'a')
                f.write(str(data)+'\n')
                f.close()'''
                print(data)
        pass


for i in range(1, 8):
    response = requests.get('https://habrahabr.ru/hubs/page'+str(i))
    comp = re.compile(r'<ul class="content-list content-list_hubs" id="hubs">(\S*?)</ul>')
    tag_place = response.text
    comp.findall(tag_place)
    parser = MyHTMLParser()
    parser.feed(tag_place)
